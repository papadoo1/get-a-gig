delimiter $$

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `account_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL COMMENT '''venue'' or ''artist''',
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Basic user information'$$

delimiter $$

CREATE TABLE `artists` (
  `artist_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'f_key',
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `genre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_members` smallint(6) DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` double DEFAULT NULL,
  `verified` tinyint(1) NOT NULL DEFAULT '0',
  `gig_count` int(11) NOT NULL DEFAULT '0',
  `profile_picture` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`artist_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `artists_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Table with specific artist details'$$


delimiter $$

CREATE TABLE `connections` (
  `connections_id` int(11) NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `status` int(11) DEFAULT '0',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`connections_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='stores the connections'$$


delimiter $$

CREATE TABLE `contacts` (
  `contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `address_1` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'optional',
  `email_1` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'different from user email',
  `email_2` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'optional, different from user email',
  `postcode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone_1` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone_2` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'optional',
  `user_id` int(11) NOT NULL COMMENT 'f_key',
  PRIMARY KEY (`contact_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='general contact details'$$


delimiter $$

CREATE TABLE `geocodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(100) NOT NULL,
  `lng` float(10,6) NOT NULL,
  `lat` float(10,6) NOT NULL,
  `address1` varchar(255) DEFAULT NULL,
  `address2` varchar(255) DEFAULT NULL,
  `address3` varchar(255) DEFAULT NULL,
  `address4` varchar(255) DEFAULT NULL,
  `town` varchar(90) DEFAULT NULL,
  `postcode` varchar(10) DEFAULT NULL,
  `country` varchar(6) DEFAULT NULL,
  `provider` varchar(30) DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` varchar(45) NOT NULL COMMENT 'the account type (venue or artist)',
  PRIMARY KEY (`id`),
  KEY `fkuserid1` (`user_id`),
  CONSTRAINT `fkuserid1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$


delimiter $$

CREATE TABLE `posts` (
  `post_id` int(11) NOT NULL AUTO_INCREMENT,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `from_name` varchar(50) NOT NULL,
  `from_picture` varchar(220) NOT NULL,
  `from_type` varchar(10) NOT NULL,
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=latin1 COMMENT='table that stores the posts'$$

delimiter $$

CREATE TABLE `venues` (
  `venue_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT 'f_key',
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL COMMENT 'pub,club,e.t.c',
  `location` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `capacity` int(11) NOT NULL,
  `opening_times` text COLLATE utf8_unicode_ci,
  `rating` double DEFAULT '0',
  `profile_picture` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`venue_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `venues_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci$$


SELECT * FROM gag_db.posts;
